
import {useState, useEffect} from 'react';

import {Table, Button} from 'react-bootstrap';




function Pagina3() {

    const [datos, setDatos] = useState([]);

    function cargaDatos(){
        fetch("http://api1.ricardhernandez.com/api/alumnes")
        .then(datos => datos.json())
        .then(x => setDatos(x) )
        .catch(() => console.log("algo falla"));
    }

    function borrar (fila_a_borrar){
        console.log("vamos a borrar:" + fila_a_borrar);
    
        const api_endpoint = "http://api1.ricardhernandez.com/api/alumnes/" + fila_a_borrar; 
        const opciones = { method: "DELETE"};
      
        fetch(api_endpoint, opciones)
        .then((x) => cargaDatos())
        
    }

    useEffect(()=>{
        cargaDatos();
    }, [])

    let filas = datos.map(alumno => (
        <tr >
            <td>{alumno.id}</td>
            <td>{alumno.nom}</td>
            <td>{alumno.email}</td>
            <td><Button onClick={()=>borrar(alumno.id)} >Borrar</Button></td>
        </tr>
    ))

    return (
        <>
            <h1>Pagina 3</h1>
            <img src="/uploads/vespa2.png" width="100px" alt="dd" />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>



        </>
    );
}

export default Pagina3;
