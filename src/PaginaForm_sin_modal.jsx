
import { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';


const validateEmail = (correo) => {
    return String(correo)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};


function PaginaForm() {

    const [nombre, setNombre] = useState('');
    const [email, setEmail] = useState('hola@xxx.com');
    const [pass1, setPass1] = useState('');
    const [pass2, setPass2] = useState('');
    const [ok, setOk] = useState(false);
    const [mensajeEmail, setMensajeEmail] = useState('');
    const [mensajeNombre, setMensajeNombre] = useState('');
    const [mensajePassword, setMensajePassword] = useState('');

    const [mostrarPasswords, setMostrarPasswords] = useState(false);

    function enviaFormulario(e) {
        e.preventDefault();
        let todoCorrecto = true;

        if (!validateEmail(email)) {
            setMensajeEmail("error, email incorrecto");
            todoCorrecto = false;
        } else {
            setMensajeEmail("");
        }

        if (nombre.includes(" ")) {
            setMensajeNombre("error, nombre incorrecto");
            todoCorrecto = false;
        } else {
            setMensajeNombre("");
        }

        if (pass1 !== pass2) {
            setMensajePassword("error, passwords no coinciden");
            todoCorrecto = false;
        } else {
            setMensajePassword("");
        }


        if (todoCorrecto) {
            setOk(true);
        } else {
            alert("hay algun fallo");
        }
    }


    if (ok) {
        return <Navigate to="/welcome" />;
    }


    return (<>
        <Form onSubmit={enviaFormulario} >
            <Form.Group className="mb-3" controlId="formBasicName">
                <Form.Label>Nombre</Form.Label>
                <Form.Control required value={nombre} onInput={(e) => setNombre(e.target.value)} type="text" />
                <Form.Text>
                    {mensajeNombre}
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control value={email} onInput={(e) => setEmail(e.target.value)} type="text" placeholder="Enter email" />
                <Form.Text>
                    {mensajeEmail}
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicName">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    value={pass1}
                    onInput={(e) => setPass1(e.target.value)}
                    type={mostrarPasswords ? "text" : "password"}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicName">
                <Form.Label>Pepetir password</Form.Label>
                <Form.Control value={pass2}
                onInput={(e) => setPass2(e.target.value)}
                type={mostrarPasswords ? "text" : "password"}
                />
                <Form.Text>
                    {mensajePassword}
                </Form.Text>
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
            <Button type="button" onClick={()=>setMostrarPasswords(!mostrarPasswords)} > passwords on/off
            </Button>
        </Form>
    </>);

}


export default PaginaForm;