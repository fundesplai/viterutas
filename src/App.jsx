import { useState } from 'react'
import './App.css'

import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { Container, Row, Col, Navbar, Nav, Button, Modal, Form } from 'react-bootstrap';

import Pagina1 from './Pagina1';
import Pagina2 from './Pagina2';
import Pagina3 from './Pagina3';
import PaginaForm from './PaginaForm';
import Cliente from './Cliente';
import Welcome from './Welcome';


function App() {
  const [count, setCount] = useState(0)


  const [show, setShow] = useState(false);

    const [nombre, setNombre] = useState('');
    const [email, setEmail] = useState('hola@xxx.com');
    const [pass1, setPass1] = useState('');
    const [pass2, setPass2] = useState('');
    const [ok, setOk] = useState(false);
    const [mensajeEmail, setMensajeEmail] = useState('');
    const [mensajeNombre, setMensajeNombre] = useState('');
    const [mensajePassword, setMensajePassword] = useState('');

    const [mostrarPasswords, setMostrarPasswords] = useState(false);

    function enviaFormulario(e) {
        e.preventDefault();
        let todoCorrecto = true;

        if (!validateEmail(email)) {
            setMensajeEmail("error, email incorrecto");
            todoCorrecto = false;
        } else {
            setMensajeEmail("");
        }

        if (nombre.includes(" ")) {
            setMensajeNombre("error, nombre incorrecto");
            todoCorrecto = false;
        } else {
            setMensajeNombre("");
        }

        if (pass1 !== pass2) {
            setMensajePassword("error, passwords no coinciden");
            todoCorrecto = false;
        } else {
            setMensajePassword("");
        }


        if (todoCorrecto) {
            setOk(true);
        } 
    }


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);




  return (
    <BrowserRouter>
      <Container>
        <Row>
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="#home">MIAPP</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                  <Link className="nav-link" to="/">Home</Link>
                  <Link className="nav-link" to="/pagina2">Pagina2</Link>
                  <Link className="nav-link" to="/pagina3">Pagina3</Link>
                  <Link className="nav-link" to="/paginaform">Form</Link>
                  <Link className="nav-link" to="/cliente">Cliente</Link>
                </Nav>
              </Navbar.Collapse>
              <Button className="justify-content-end" variant="primary" onClick={handleShow}>
                    Mostrar registro
                </Button>
            </Container>
          </Navbar>
        </Row>
       
        <Row>
          <Col xs="3" className="bg-primary">

          </Col>
          <Col className="bg-warning">
            <Routes>
              <Route path="/" element={<Pagina1 />} />
              <Route path="/pagina2" element={<Pagina2 />} />
              <Route path="/pagina3" element={<Pagina3 />} />
              <Route path="/paginaform" element={<PaginaForm />} />
              <Route path="/cliente" element={<Cliente />} />
              <Route path="/welcome" element={<Welcome />} />
            </Routes>
          </Col>
        </Row>
      </Container>


      <Modal show={show} onHide={handleClose}>
            <Form onSubmit={enviaFormulario} >
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form.Group className="mb-3" controlId="formBasicName">
                        <Form.Label>Nombre</Form.Label>
                        <Form.Control required value={nombre} onInput={(e) => setNombre(e.target.value)} type="text" />
                        <Form.Text>
                            {mensajeNombre}
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control value={email} onInput={(e) => setEmail(e.target.value)} type="text" placeholder="Enter email" />
                        <Form.Text>
                            {mensajeEmail}
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicName">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            value={pass1}
                            onInput={(e) => setPass1(e.target.value)}
                            type={mostrarPasswords ? "text" : "password"}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicName">
                        <Form.Label>Pepetir password</Form.Label>
                        <Form.Control value={pass2}
                            onInput={(e) => setPass2(e.target.value)}
                            type={mostrarPasswords ? "text" : "password"}
                        />
                        <Form.Text>
                            {mensajePassword}
                        </Form.Text>
                    </Form.Group>

                    <Button type="button" onClick={() => setMostrarPasswords(!mostrarPasswords)} > passwords on/off
                    </Button>


                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Modal.Footer>

            </Form>
            </Modal>

    </BrowserRouter>
  )
}

export default App
